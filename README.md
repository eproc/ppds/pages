# Getting Started with Public Procurement Data Space (PPDS) Gitlab

Welcome to the Public Procurement Data Space (PPDS) Gitlab! This documentation provides comprehensive information about our project, indicators, and data quality.

## Prerequisites

Before you get started, please ensure that you have the following prerequisites installed:

1. **Git:** You need Git to clone the repository and manage the documentation files. You can download Git from [here](https://git-scm.com/).

2. **Python:** The documentation is built using MkDocs, which requires Python. You can download Python from [here](https://www.python.org/downloads/).

3. **MkDocs:** PPDS Gitlab documentation uses the MkDocs sites generator. You can follow the installation instructions [here](https://www.mkdocs.org/getting-started/)

4. **MkDocs Material:** PPDS Gitlab documentation uses the MkDocs Material theme. You can follow the installation instructions [here](https://squidfunk.github.io/mkdocs-material/getting-started/) to install MkDocs Material.

## Migrating GitLab Pages to a Different Repository

If you need to migrate your GitLab Pages to a different repository, follow these steps:

1. **Create a New Repository:**

   - Create a new GitLab repository where you want to host your documentation.

2. **Clone the New Repository:**

   - Clone the new repository to your local machine.

3. **Copy Your Documentation Files:**

   - Copy the contents of the `public` directory from your current GitLab Pages repository to the new repository.

4. **Update URLs:**

   - In your documentation files, update any URLs or links that point to the old repository to now point to the new repository.

5. **Push Changes:**

   - Commit and push your changes to the new repository.

6. **Configure GitLab Pages:**

   - In the new repository, configure GitLab Pages in the repository settings to use the `public` directory as the source for your GitLab Pages site.

7. **Access Your Documentation:**

   - Your documentation should now be accessible via GitLab Pages in the new repository.



