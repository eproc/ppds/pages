## **Welcome to the Public Procurement Data Space (PPDS) GitLab!**


The European Commission is introducing the Public Procurement Data Space (PPDS), which will be the first EU-wide platform for accessing public procurement data scattered across EU, national, and regional levels. The PPDS will improve data quality, availability and completeness by using the new eForms that allow public buyers to provide information in a more structured way. The wealth of data from PPDS will be combined with advanced analytics technologies, including Artificial Intelligence (AI), facilitating the access to public procurement data, providing better value for money for public buyers, and promoting a green, social and innovative economy.

The PPDS has been materialised officially through the [Communication from the Commission Public Procurement: A data space to improve public spending, boost data-driven policy-making and improve access to tenders for SMEs.](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A52023XC0316%2802%29&qid=1679492590667)

The objective of this GitLab is to enhance the collaboration between the PPDS participants, exchange documentation about the PPDS, both at the business and technical levels, collect feedback from the stakeholders on the PPDS, and share the source code of PPDS tools and components. The GitLab is organised as follows:

* [PPDS Indicators Repository](indicators.md). This repository provides explanations and documentation on how the analytics and indicators, especially the Single Market Scoreboard Indicators, are calculated.

* [Data Quality Business Rules](data_quality.md). This repository provides documentation and information regarding the data quality rules applied within the PPDS to ensure the good quality of the data regarding validity, completeness and consistency.

* [Change Requests & Feedback.](https://code.europa.eu/eproc/ppds/crs/-/wikis/home) This is the official channel for gathering feedback and comments on the PPDS. If you would like to provide any feedback, please share it with us through this channel.





[Get in touch with us!](mailto:EC-PPDS@ec.europa.eu?subject=PPDS - contact){ .md-button }