#### Description

This indicator measures the **proportion of procurement procedures that are divided into lots over the total amount of procurement procedures**.

#### Methodology

1.	The number of Lots for each CN is calculated.

2.	The number of CNs which have more than one Lot is calculated for every dimension (numerator of Indicator).

3.	The total number of CNs is calculated for every dimension (denominator of Indicator).

4.	A division of the numerator over the denominator calculated in the previous step is made for every dimension. This will return the proportion of Contracts divided into Lots by dimension.


#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the Procedure | `#hasIdentifierValue` |
| **Lot ID**                       | Identifier of the lot within each contract award notice                          | `#hasIdentifierValue (from lot)`                                                 |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |

#### Data Quality Checks Used

| **Business Rule**                                                                         | **Business Rule Category** | **ePO Attribute**                                                               |
|-------------------------------------------------------------------------------------------|----------------------------|---------------------------------------------------------------------------------|
| **Information on whether the procurement procedure divided into lots is provided**        | Completeness               | `#hasProcurementScopeDividedIntoLot`                                            |                                                       |
| **Amount of lots a contract is divided into should be within a reasonable range (e.g., below 500 lots for Supplies, and below 50 lots for Works and Services)** | Accuracy                   | `#hasIdentifierValue`                                                           |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
