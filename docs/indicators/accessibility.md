#### Description
This indicator measures the proportion of procurement procedures with focus on accessibility. It must be taken into account that only data from eForms is used in this indicator.

#### Methodology
