

#### Description
This indicator measures the proportion of procurement procedures with strategic procurement. It must be taken into account that only data from eForms is used in this indicator.


#### Methodology

1.	The data is filtered to include only eForms data, since information related to strategic procurement is not available in other data sources at the moment. In reality, this indicator is not only a percentage, but a set of them taking into account various factors and categories. The calculations made to popularize the Strategic Procurement dashboard will be explained below. 

2.	The total number of lots is calculated for every dimension (common denominator for all the KPIs).

3.	 The number of lots that have the Strategic Procurement field informed in eForms is calculated for every dimension .  A division of this number by the denominator will return the Rate of informed Strategic Procurement. 

4.	The number of lots with Strategic Procurement is calculated for every dimension. A division of this number by the denominator will return the Rate of Strategic Procurement. 

5.	 For the 3 types of strategic procurement, the number of lots for each type is calculated for every dimension. A division of this number by the denominator will return three KPIs: 

    - Rate of fulfilment of social objectives 
    - Rate of innovative purchase 
    - Rate of reduction of environmental impacts


#### **Data Fields Used**
| **Data** |**Description**|**ePO Property** |
| ------ | ------ | ------ |
| **Procedure ID** | Identifier of the Procedure |`#hasIdentifierValue`|
|**Lot ID**|Identifier of the lot within each contract award notice        |`#hasIdentifierValue (from lot)`|
|**Year**|Year in which the contract was awarded|There is no ePO property that provides this information directly, but it is derived from #hasDispatchDate|
|**Month**|Month in which the contract was awarded |There is no ePO property that provides this information directly, but it is derived from #hasDispatchDate|
|**Country**|Contracting authority country|`#hasCountryCode (from Main Buyer)`|
|**NUTS Code**| 	Common classification of territorial units for statistics	|`#hasNutsCode (from Main Buyer)`|
|**Procedure Type**|	Procurement method used for the award of the contract	|`#hasProcedureType`|
|**Contract Type**|	Information about the contract type (goods, services, works)|	`#hasContractNatureType`|
|**Directive Applicable**| 	The legal basis under which the procurement procedure takes place|	`#hasLegalBasis`
|**Buyer Type**| 	Information about the type of contracting authority (e.g., central government, regional, etc.)|	`#hasBuyerLegalType`|
|**Buyer Main Activity**|	The principal sectoral area in which an organisation operates	|`#hasMainActivity`
|**CPV Code**|	Information about the CPV code used, kept at the division level (2 digits)	|`#hasMainClassification`
|**Lot Division**|Indicator that shows whether a contract has been divided into lots or not|There is no ePO property that provides this information directly, but it is derived from #hasProcurementScopeDividedIntoLot|
|**Estimated Value**|  |There is no ePO property that provides this information directly, but it is derived from  #hasAmountValue|
|**Estimated Value Range**| Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.)| There is no ePO property that provides this information directly, but it is derived from #hasTotalValue|
|**EU Threshold**|Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory|There is no ePO property that provides this information directly, but it is derived from #hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis|
|**Data Source**|  |It’s not a ePO property, but a DcTerms property  called #conformsTo|
|**Strategic Procurement Type**|  |`#fulfillsStrategicProcurement`|
|**Strategic Procurement Subtype**|  |       `#fulfillsRequirement`|

#### **Data Quality Checks Used**
This indicator uses only the common filter for all indicators described in the document Changes PPDS Gitlab.

#### **Source of data**
CN data graph including only eForms Data.

#### **Unit of measure**
Percentage