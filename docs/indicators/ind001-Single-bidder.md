
#### Description

This indicator measures the **proportion of contracts awarded where there was just a single bidder** (excluding framework agreements, as they have different reporting patterns; direct awards, i.e., negotiated without a call for competition/award without prior publication of a contract notice, are also excluded, since for such procedures, the legislator did not foresee competition).

#### Methodology

1.	The data is filtered to exclude the following cases:

    - Framework Agreements. The reason to exclude framework agreements lies in the fact that the reporting of framework agreements and the contracts based on framework agreements is not equal and harmonized across countries. Therefore, including them could cause a significant bias in the indicator results.

    - Procedures whose all lots are not awarded.

    - Procedures in which procedure type is not "Negotiated procedure without a call for competition" ("neg-wo-call" in ePO). Those types of Procedures are excluded due to the nature of the Contract.

2.	The number of lots where there was only one tender is calculated for every dimension (numerator of Indicator). For counting the number of tenders, the following methodology is used:

    - If epo:hasReceivedTenders is informed, this value is taken as the number of tenders. 

    - If epo:hasReceivedTenders is not informed, th value considered is epo:hasElectronicTenders. 

    - If none of them is informed, then the lot is discarded from the indicator calculation.

3.	The total number of lots is calculated for every dimension (denominator of Indicator). 

4.	A division of the numerator by the denominator calculated in the previous step is made for every dimension. This will return the proportion of contract awards where there was a single bidder in each dimension.



#### Data Fields Used

| **Data** | **Description** | **ePO Property** |
| --- | --- | --- |
| **Procedure ID** | Identifier of the procedure | `#hasIdentifierValue` |
| **Lot ID** | Identifier of the lot within each contract award notice | `#hasIdentifierValue (from lot)` |
| **Year** | Year in which the contract was awarded | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month** | Month in which the contract was awarded | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country** | Contracting authority country | `#hasCountryCode (from Main Buyer)` |
| **NUTS Code** | Common classification of territorial units for statistics | `#hasNutsCode (from Main Buyer)` |
| **Procedure Type** | Procurement method used for the award of the contract | `#hasProcedureType` |
| **Contract Type** | Information about the contract type (goods, services, works) | `#hasContractNatureType` |
| **Legal Basis** | The legal basis under which the procurement procedure takes place | `#hasLegalBasis` |
| **Contracting Authority Type** | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#hasBuyerLegalType` |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates | `#hasMainActivity` |
| **CPV Code** | Information about the CPV code used, kept at the division level (2 digits) | `#hasMainClassification` |
| **EU Threshold** | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range** | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division** | Indicator that shows whether a contract has been divided into lots or not | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement** | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm` |
| **Amount of Tenders** | Number of tenderers submitting offers to a contract | `#hasReceivedTenders (for every Award)` |
| **Contract Awarded** | Indicates whether the lot is awarded, not awarded, or still open | `#hasAwardStatus` |

#### Data Quality Checks

The following data quality checks are performed:

| **Check** | **Business Rule Category** | **ePO Property** |
| --- | --- | --- |
| **Number of received tenders is provided** | Completeness | `#hasReceivedTenders` |
| **Number of received tenders is ≥ 0** | Validity | `#hasReceivedTenders` |
| **If contract is awarded, number of received tenders must be ≥ 1** | Consistency | `#hasAwardStatus #hasReceivedTenders` |
| **Amount of Tenders Received ≥ Amount of SME Tenders** | Consistency | `#hasReceivedTenders #hasReceivedSMETenders` |
| **Amount of Tenders Received should be within a reasonable range (e.g., below 250 bids)** | Accuracy | `#hasReceivedTenders` |

#### Source of Data

Mainly CAN (Contract Award Notice) data graph.

#### Unit of Measure

Percentage
