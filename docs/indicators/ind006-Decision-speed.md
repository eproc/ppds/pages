#### Description

This indicator measures the **mean decision-making period**, i.e. the time between the deadline for receiving offers and the date the contract is awarded. To ensure comparability, only notices published under the open procedure, which do not include framework agreements, are considered.

#### Methodology

1.	The data is filtered to exclude the following cases:
    - Framework Agreements.
    - Procedures whose all lots are not awarded.

2.	 The difference (in days) between the Award Date and the Submission deadline date is calculated for every Award. This number is calculated using the following ePO fields:
    - The epo:hasContractConclusionDate from the CAN.
    - The epo:hasReceiptDeadline from the CN.

3.	 The difference (in days) between the Award Date and the Submission deadline date across all Awards is calculated for every dimension. This will return the Award decision-making period by dimension at Award level.

4.	The mean of the Award decision-making period is calculated for every dimension. This will return the average decision-making period by dimension.


#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the procedure | `#hasIdentifierValue` |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **Date of Conclusion of the Contract** | Date when a contract is awarded                                       | `#hasContractConclusionDate (for every award)`                                    |
| **Deadline for Receipt of Tenders** | Information on the maximum date for the submission of offers by the potential tenderers | `#hasReceiptDeadline`                                                            |

#### Data Quality Checks Used

| **Business Rule**                                           | **Business Rule Category** | **ePO Attribute**                                                    |
|-------------------------------------------------------------|----------------------------|----------------------------------------------------------------------|
| **Procedure type is provided**                              | Completeness               | `#hasProcedureType`                                                  |                                             |
| **Contract conclusion date is provided**          | Completeness               | `#hasContractConclusionDate`                                         |                                               |                                          |
| **Contract conclusion date has a proper date format** | Validity                 | `#hasContractConclusionDate`                                         |
| **The dispatch date of the CN is prior to the Deadline for submission of tenders** | Consistency           | `#hasDispatchDate #hasReceiptDeadline`                               |                        |
|**Dispatch date of the CN < Tenders receipt deadline** | Consistency | `#hasDispatchDate #hasContractConclusionDate`
| **Dispatch date of the CAN < Contract conclusion date** | Consistency            | `#hasReceiptDeadline #hasDispatchDate`                               |
| **Tenders receipt deadline should be within a reasonable range (e.g. between 1 week and 2 months** | Accuracy           | `#hasReceiptDeadline`                                                |

#### Source of Data

CN and CAN data graph

#### Unit of Measure

Days
