#### Description

This indicator measures the **proportion of procurement procedures with more than one public buyer**, including both joint procurement and procurement conducted by a Central Purchasing Body (CPB).

#### Methodology

1.	The number of CNs in which the fields "Is joint procurement" or "Procedure is managed by CPB" are set to "TRUE" is calculated for every dimension (numerator of Indicator).

2.	The total number of CNs is calculated for every dimension (denominator of Indicator).

3.	A division of the numerator over the denominator calculated in the previous step is made for every dimension. This will return the proportion of CNs over the total CNs where there was joint procurement by dimension.



#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the procedure | `#hasIdentifierValue` |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                               | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **Is Joint Procurement**         | Information about whether the contract is awarded by two or more contracting authorities | `#isJointProcurement`                                                            |
| **Procedure is Managed by CPB**  | Information about whether the contract is awarded by a centralised purchasing body | `#isAwardedByCPB`                                                               |
| **National Registration Number of the Buyer** | Registration number of the buyer / contracting authority                   | `#hasIdentifierValue (from all buyers, i.e., main buyer and additional buyers)`   |

#### Data Quality Checks Used

| **Business Rule**                                           | **Business Rule Category** | **ePO Attribute**                                                    |
|-------------------------------------------------------------|----------------------------|----------------------------------------------------------------------|
| **Information on whether the procurement procedure is joint procurement is provided**                        | Completeness               | `#isJointProcurement`                                                |
| **National Registration Number of the Buyer is provided**   | Completeness               | `#hasIdentifierValue (from all buyers, i.e., main buyer and additional buyers)` |
| **Joint procurement follows a Boolean format**              | Validity                   | `#isJointProcurement`                                                |
| **National Registration Number of the Buyer is valid**      | Validity                   | `#hasIdentifierValue (from all buyers, i.e., main buyer and additional buyers)` |
| **In case of joint procurement, more than one contracting entity (in the role of buyer) needs to be provided** | Consistency               | `#isJointProcurement #isContractingEntity`                           |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
