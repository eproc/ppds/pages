#### Description

This indicator measures the proportion of contracts awarded after a call for tender for which the CN number is not provided, over the total amount of contracts.

#### Methodology

1. The data is filtered to exclude the following cases:

    - Framework Agreements. This is because it is not enforced to include the CN identifier in all the CANs that are part of the same Framework Agreement. It is necessary to include the CN identifier in at least one CAN from the set of CANs corresponding to the same Framework Agreement.

    - Procedures whose all lots are not awarded.

    - Procedures in which procedure type is not "Negotiated procedure without a call for competition" ("neg-wo-call" in ePO). Those types of Procedures are treated differently because, due to the nature of the Contract, it is not necessary to provide a CN identifier for those Contracts.

2.	The number of CANs in which the CN identifier related to the CAN is missing is calculated for every dimension (numerator of Indicator).

3.	The total number of CANs is calculated for every dimension (denominator of Indicator).

4.	A division of the numerator over the denominator calculated in the previous step is made for every dimension. This will return the proportion of CANs without its CN information by dimension.


#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the procedure | `#hasIdentifierValue` |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from main buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **CN Identifier**                | Identifier of the Contract Notice associated to a Contract Award Notice         | `#hasIdentifierValue (from CN reference in the CAN)`                             |

#### Data Quality Checks Used

| **Business Rule**                                                     | **Business Rule Category** | **ePO Attribute**            |
|-----------------------------------------------------------------------|----------------------------|------------------------------|
| **Information on whether the contract is FA is provided**             | Completeness               | `#FrameworkAgreementTerm`     |
| **Procedure type is provided**                                        | Completeness               | `#hasProcedureType`           |
| **Procurement procedure type is valid as defined by EU Vocabularies** | Validity                   | `#hasProcedureType`           |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
