#### Description

This indicator measures the **proportion of contracts that are awarded to Small and Medium-sized Enterprises (SME) over the total number of contracts**.

#### Methodology

1.	The data is filtered to exclude the following cases:

    -  Procedures whose all lots are not awarded.
2.	The number of awards where at least one winning company is an SME is calculated (numerator of Indicator). Its calculation is done by checking whether the business size of the contractor is an SME, or if the contractor is a group of companies, at least one of them is an SME.

3.	The total number of awards is calculated for every dimension (denominator of Indicator).

4.	A division of the numerator by the denominator calculated in the previous step is made for every dimension. This will return the proportion of contract awards where there was at least one SME contractor in each dimension.


#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the contract procedure | `#hasIdentifierValue` |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **Information on Whether the Contractor is an SME is Provided** | Information on whether the awarded contractor is an SME or not              | `#hasBusinessSize (for every award)`                                              |
| **Amount of SME Tenders**        | Amount of tenderers submitting offers to a tender that are SMEs                | `#hasReceivedSMETenders (for every award)`                                       |
| **Amount of Tenders**            | Number of tenderers submitting offers to a contract                             | `#hasReceivedTenders (for every award)`                                          |

#### Data Quality Checks Used

| **Business Rule**                                                     | **Business Rule Category** | **ePO Attribute**         |
|-----------------------------------------------------------------------|----------------------------|---------------------------|
| **Information on whether the contractor is an SME (business size) is provided**       | Completeness               | `#hasBusinessSize`        |
| **Number of received SME tenders is provided**                        | Completeness               | `#hasReceivedSMETenders`  |
| **Number of received tenders is provided**                            | Completeness               | `#hasReceivedTenders`     |
| **Amount of tenders received is ≥ 0**                                 | Validity                   | `#hasReceivedTenders`     |
| **Amount of SME tenders received is ≥ 0**                             | Validity                   | `#hasReceivedSMETenders`  |
| **The contractor is an SME is valid as defined by EU Vocabularies (“Code” column)**                 | Validity                   | `#hasBusinessSize`        |
| **If winner is SME (business size), number of received SME tenders must be ≥ 1**      | Consistency                | `#hasBusinessSize #hasReceivedSMETenders` |
| **Amount of tenders received should be within a reasonable range (e.g., below 250 bids)** | Accuracy                  | `#hasIdentifierValue`     |
| **Amount of received SME tenders should be within a reasonable range (e.g., below 250 bids)** | Accuracy                  | `#hasReceivedSMETenders`  |
| **If winner is a SME, total value of the contract (Contract Award Value) should be within a reasonable range (e.g. below 5,000,000€)** | Accuracy           | `#hasBusinessSize #hasTotalValue` |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
