**Description**

this Business Rules are common to all Data Quality Indicators.

|Business Rule| Business Rule category | ePO Attribute |
| ------ | ------ | ------ |
|**Contract ID is provided**| Completeness|`#hasID`|
|**Lot ID is provided**| Completeness|`#hasID`|
|**Main activity of the Buyer is provided**| Completeness|`#hasMainActivity`|
|**Country code of the Buyer is provided**| Completeness|`#hasID`|
|**Procedure type is provided**| Completeness|`#hasProcedureType`|
|**Currency code value is provided**| Completeness|`#hasCurrency`|
|**Country code is valid as defined by EU Vocabularies (“Code” column)**| Validity|`#hasCountryCode`|
|**Procedure type is valid as defined by EU Vocabularies (“Code” column)**| Validity|`#hasProcedureType`|
|**Contract nature type is valid as defined by EU Vocabularies (“Code” column)**| Validity|`#hasContractNatureType`|
|**Main classification (CPV code) is valid as defined by EU Vocabularies**| Validity|`#hasMainClassification`|
|**Currency code is valid as defined by EU Vocabularies (“Code” column)**| Validity|`#hasCurrencyCode`|
|**An amount value ≥ 10**| Validity|`#hasAmountValue`|
|**Dispatch date has a proper date format**| Validity|`#hasDispatchDate`|