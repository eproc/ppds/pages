#### Description

This indicator measures the **average proportion of SME bids over the total number of bids**.

#### Methodology

1.	The data is filtered to exclude the following cases:

    - Procedures whose all lots are not awarded.

2.	The number of tenders for each Lot is calculated. For counting the number of tenders, the following methodology is used:

    - If epo:hasReceivedTenders is informed, this value is taken as the number of tenders. 

    - If epo:hasReceivedTenders is not informed, th value considered is epo:hasElectronicTenders. 

    - If none of them is informed, then the lot is discarded from the indicator calculation.

3.	The number of SME tenders for each Lot is calculated. This value is calculated using the property epo:hasSMETenders.

4.	The proportion of SME Tenders for every Lot is calculated. This is done by dividing the “Number of SME tenders" by the "Number of tenders" for every Lot.

5.	The mean of the proportions of SME Tenders of every Lot is calculated for every dimension. This will return the average proportion of SME Tenders by dimension.



#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the procedure | `#hasIdentifierValue` |
| **Lot ID**                       | Identifier of the lot within each contract Award notice                          | `#hasIdentifierValue (from lot)`                                                 |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the Award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Legal Basis**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Contracting Authority Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#Buyer`                                                                         |
| **Contracting Authority Sector** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **Amount of Tenders**            | Number of tenderers submitting offers to a contract                             | `#hasReceivedTenders (for every Award)`                                          |
| **Information on Whether the Contractor is an SME is Provided** | Information on whether the awarded contractor is an SME or not              | `#hasBusinessSize (for every Award)`                                              |
| **Amount of SME Tenders**        | Amount of tenderers submitting offers to a tender that are SMEs                | `#hasReceivedSMETenders (for every Award)`                                       |
| **Contract Awarded**             | Indicates whether the lot is awarded, not awarded, or still open               | `#hasAwardStatus`                                                                |

#### Data Quality Checks Used

| **Business Rule**                                                 | **Business Rule Category** | **ePO Attribute**          |
|-------------------------------------------------------------------|----------------------------|----------------------------|
| **Amount of received SME tenders is provided**                    | Completeness               | `#hasReceivedSMETenders`   |
| **Amount of tenders received is ≥ 0**                             | Validity                   | `#hasReceivedTenders`      |
| **Amount of SME tenders received is ≥ 0**                         | Validity                   | `#hasReceivedSMETenders`   |
| **If winner is SME, amount of received SME tenders must be ≥ 1**  | Consistency                | `#hasBusinessSize #hasReceivedSMETenders` |
| **If the contract is awarded, amount of received tenders must be ≥ 1** | Consistency                | `#hasAwardStatus #hasReceivedTenders` |
| **Amount of tenders received ≥ amount of SME tenders**            | Consistency                | `#hasReceivedTenders #hasReceivedSMETenders` |
| **Amount of received SME tenders should be within a reasonable range (e.g., below 250 bids)** | Accuracy                  | `#hasReceivedSMETenders`   |
| **Amount of tenders received should be within a reasonable range (e.g., below 250 bids)** | Accuracy                  | `#hasReceivedTenders`      |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
