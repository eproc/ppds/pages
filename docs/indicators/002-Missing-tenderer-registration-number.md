#### Description

This indicator measures the proportion of contractors that did not include the National Registration Number, over the total amount of contractors.

#### Methodology

1.	The data is filtered to exclude the following cases:

    - Procedures whose all lots are not awarded.

2.	The number of Contractors in which the National Registration Number is not provided is calculated for every dimension (numerator of Indicator).

3.	The total number of Contractors is calculated for every dimension (denominator of Indicator).

4.	A division of the numerator over the denominator calculated in the previous step is made for every dimension. This will return the proportion of awards whose winner has not provided the National Registration Number, by dimension.



#### Data Fields Used

| **Data**                         | **Description**                                                                 | **ePO Property**                                                                 |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------------------------------------------|
| **Procedure ID**                  | Identifier of the procedure | `#refersToProcedure` |
| **Year**                         | Year in which the contract was awarded                                          | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Month**                        | Month in which the contract was awarded                                         | There is no ePO property that provides this information directly, but it is derived from `#hasDispatchDate` |
| **Country**                      | Contracting authority country                                                  | `#hasCountryCode (from Main Buyer)`                                              |
| **NUTS Code**                         | Common classification of territorial units for statistics                                                 | `#hasNutsCode (from Main Buyer)`                                                 |
| **Procedure Type**               | Procurement method used for the award of the contract                           | `#hasProcedureType`                                                              |
| **Contract Type**                | Information about the contract type (goods, services, works)                    | `#hasContractNatureType`                                                         |
| **Directive Applicable**                  | The legal basis under which the procurement procedure takes place               | `#hasLegalBasis`                                                                 |
| **Buyer Type**   | Information about the type of contracting authority (e.g., central government, regional, etc.) | `#hasBuyerLegalType`                                                                         |
| **Buyer Main Activity** | The principal sectoral area in which an organisation operates                   | `#hasMainActivity`                                                               |
| **CPV Code**                     | Information about the CPV code used, kept at the division level (2 digits)      | `#hasMainClassification`                                                         |
| **Framework Agreement**          | Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system | `#FrameworkAgreementTerm`                                                        |
| **Lot Division**                 | Indicator that shows whether a contract has been divided into lots or not       | There is no ePO property that provides this information directly, but it is derived from `#hasProcurementScopeDividedIntoLot` |
| **Estimated Value**                  | Estimated value| There is no ePO property that provides this information directly, but it is derived from `#hasAmountValue` |
| **Estimated Value Range**                  | Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.) | There is no ePO property that provides this information directly, but it is derived from `#hasTotalValue` |
| **EU Threshold**                 | Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory | There is no ePO property that provides this information directly, but it is derived from `#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis` |
| **National Registration Number of the Winner** | Registration number of the tenderer                                   | `#hasIdentifierValue (from all awards)`                                           |

#### Data Quality Checks Used

| **Business Rule**                                                     | **Business Rule Category** | **ePO Attribute**            |
|-----------------------------------------------------------------------|----------------------------|------------------------------|
| **Country of the Winner is provided**                                 | Completeness               | `#hasCountryCode`            |
| **National Registration Number of the Winner is provided**            | Completeness               | `#hasIdentifierValue`        |

#### Source of Data

Mainly CAN data graph

#### Unit of Measure

Percentage
