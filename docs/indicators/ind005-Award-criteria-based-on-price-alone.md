#### Description

This indicator measures the **proportion of procedures awarded based solely on the price as the single award criterion**, over the total number of procedures.

#### Methodology

1. The data is filtered to exclude the following cases:
    - Procedures whose all lots are not awarded.

2. For the numerator, the following methodology is used:

    - More than one Award Criterion

        - If they are all Price Criterion -> Is Only Price.

        - If there's a criterion that is not Price Criterion -> Is not Only Price.

    - Only one Award Criterion and it's Price

        - If the weight of the criterion is 100 or empty -> Is Only Price.
        - If the weight is less than 100 -> Is not Only Price.

3.	The total number of Lots is calculated for every dimension (denominator of Indicator).

4.	A division of the numerator over the denominator calculated in the previous step is made for every dimension. This will return the proportion of Lots that were awarded only because of price by dimension.




#### Data fields used

<table>
<tr>
<th>Data</th>
<th>Description</th>
<th>ePO property</th>
</tr>
<tr>
<td>

<strong><span dir="">Procedure ID</span></strong>
</td>
<td>

<span dir="">Identifier of the procedure</span>
</td>
<td>

<span dir="">#hasIdentifierValue</span>
</tr>
<tr>
<td>

<strong><span dir="">Lot ID</span></strong>
</td>
<td>

<span dir="">Identifier of the lot within each contract award notice</span>
</td>
<td>

<span dir="">#hasIdentifierValue (from lot)</span>
</tr>
<tr>
<td>

<strong><span dir="">Year</span></strong>
</td>
<td>

<span dir="">Year in which the contract was awarded</span>
</td>
<td>

<span dir="">There is not an ePO property that provides this information directly, but it is derived from #hasDispatchDate</span>
</tr>
<tr>
<td>

<strong><span dir="">Month</span></strong>
</td>
<td>

<span dir="">Month in which the contract was awarded</span>
</td>
<td>

<span dir="">There is not an ePO property that provides this information directly, but it is derived from #hasDispatchDate</span>
</tr>
<tr>
<td>

<strong><span dir="">Country</span></strong>
</td>
<td>

<span dir="">Contracting authority country</span>
</td>
<td>

<span dir="">#hasCountryCode (from Main Buyer)</span>
</tr>
<tr>
<td>

<strong><span dir="">NUTS Code</span></strong>
</td>
<td>

<span dir="">Common classification of territorial units for statistics</span>
</td>
<td>

<span dir="">#hasNutsCode (from Main Buyer)</span>
</tr>
<tr>
<td>

<strong><span dir="">Procedure Type</span></strong>
</td>
<td>

<span dir="">Procurement method used for the award of the contract</span>
</td>
<td>

<span dir="">#hasProcedureType</span>
</tr>
<tr>
<td>

<strong><span dir="">Contract Type</span></strong>
</td>
<td>

<span dir="">Information about the contract type (goods, services, works)</span>
</td>
<td>

<span dir="">#hasContractNatureType</span>
</tr>
<tr>
<td>

<strong><span dir="">Legal basis</span></strong>
</td>
<td>

<span dir="">The legal basis under which the procurement procedure takes place </span>
</td>
<td>

<span dir="">#hasLegalBasis</span>
</tr>
<tr>
<td>

<strong><span dir="">Contracting Authority Type</span></strong>
</td>
<td>

<span dir="">Information about the type of contracting authority (e.g., central government, regional, etc.)</span>
</td>
<td>

<span dir="">#Buyer</span>
</tr>
<tr>
<td>

<strong><span dir="">Contracting Authority Sector</span></strong>
</td>
<td>

<span dir="">The principal sectoral area in which an organisation operates </span>
</td>
<td>

<span dir="">#hasMainActivity</span>
</tr>
<tr>
<td>

<strong><span dir="">CPV Code</span></strong>
</td>
<td>

<span dir="">Information about the CPV code used, kept at the division level (2 digits)</span>
</td>
<td>

<span dir="">#hasMainClassification</span>
</tr>
<tr>
<td>

<strong><span dir="">EU Threshold</span></strong>
</td>
<td>

<span dir="">Information on whether a contract is above or below the EU thresholds that establish whether the publication in TED is mandatory</span>
</td>
<td>

<span dir="">There is not an ePO property that provides this information directly, but it is derived from: 
#hasContractNatureType #hasEstimatedValue #hasBuyerLegalType #hasLegalBasis
</span>
</tr>
<tr>
<td>

<strong><span dir="">Value Range</span></strong>
</td>
<td>

<span dir="">Range of value where the contract is located (e.g., up to 15,000, from 15,000 to 50,000, etc.)</span>
</td>
<td>

<span dir="">There is not an ePO property that provides this information directly, but it is derived from #hasTotalValue</span>
</tr>
<tr>
<td>

<strong><span dir="">Lot Division</span></strong>
</td>
<td>

<span dir="">Indicator that shows whether a contract has been divided into lots or not</span>
</td>
<td>

<span dir="">There is not an ePO property that provides this information directly, but it is derived from #hasProcurementScopeDividedIntoLot</span>
</tr>
<tr>
<td>

<strong><span dir="">Framework Agreement</span></strong>
</td>
<td>

<span dir="">Information on whether the contract is aimed at establishing a framework agreement or a dynamic purchasing system</span>
</td>
<td>

<span dir="">#FrameworkAgreementTerm</span>
</tr>
<tr>
<td>

<strong><span dir="">Award Criterion</span></strong>
</td>
<td>

<span dir="">Information related to the criteria used to award the contract</span>
</td>
<td>

<span dir="">#hasAwardCriterionType</span>
</tr>
<tr>
<td>

<strong><span dir="">Weight of Award Criterion</span></strong>
</td>
<td>

<span dir="">Information related to the weights of the different criteria used to award the contract</span>
</td>
<td>

<span dir="">#weight</span>
</tr>
<tr>

</table>

#### Data Quality Checks Used

<table>
<tr>
<th>Business Rule</th>
<th>Business Rule Category</th>
<th>ePO attribute</th>
</tr>
<tr>
<td>


<strong><span dir="">Award criterion type is provided </span></strong>
</td>
<td>

<span dir="">Completeness</span>
</td>
<td>

<span dir="">#hasAwardCriterionType</span>
</tr>
<tr>
<td>

<strong><span dir="">Award Criteria Weight is provided</span></strong>
</td>
<td>

<span dir="">Completeness</span>
</td>
<td>

<span dir="">#weight</span>
</tr>
<tr>

</table>

#### Source of data

Mainly CAN data graph

#### Unit of measure

Percentage