## **PPDS Analytics & Indicators**

This repository contains information about the methodology used for the analytics and indicators calculators within the PPDS. 

### How to read and navigate the indicators list
The indicators in this document are organized into thematic groups, each assigned a three-digit group number (e.g., "001: Data Quality"). Within each group, individual indicators are also numbered with a three-digit identifier, starting from 001. This structure helps in categorizing and referencing the indicators systematically.

**Understanding the Numbers:**
- **Group Number:** The first three digits before the colon represent the group to which the indicators belong (e.g., **001** for Data Quality).
- **Indicator Number:** The subsequent three-digit number after the group title refers to the specific indicator within that group (e.g., **001** for "Missing link to call for competition" in the Data Quality group).

For example, "003: Competition" is a group focusing on competition aspects within public procurement. Within this group, "001: Single bidder" is the specific indicator related to procedures with only 1 bidder.


### Indicators description
For a specific description of each indicator, go to the pages below.

**001: Data Quality**

- [001: Missing previous publication number](indicators/001-Missing-previous-publication-number.md)
- [002: Missing tenderer registration number](indicators/002-Missing-tenderer-registration-number.md)
- [003: Missing buyer registration number](indicators/003-Missing-buyer-registration-number.md)

**002: Public Procurement Overview**

- [001: Procedures divided into lots](indicators/005-Procedures-divided-into-lots.md)
- [002: Total number of tenders](indicators/Total-number-of-tenders.md)
- [003: Total value of tenders](indicators/Total-value-of-tenders.md)
- [004: Total number of awarded contracts](indicators/Total-number-awarded-contracts.md)
- [005: Total value of awarded contracts](indicators/Total-value-of-awarded-contracts.md)
- [006: Analysis of contracting authorities by number of tenders](indicators/Ranking-of-contracting-authorities-by-number-of-tenders-and-by-procurement-value.md)
- [008: Analysis of contracting authorities by procurement value](indicators/Ranking-of-contracting-authorities-by-number-of-tenders-and-by-procurement-value.md)
- [009: Analysis of economic operators by number of contracts](indicators/Ranking-of-economic-operators-by-number-of-tenders-and-by-procurement-value.md)
- [010: Analysis of economic operators by value of contracts](indicators/Ranking-of-economic-operators-by-number-of-tenders-and-by-procurement-value.md)

**003: Competition**

- [001: Single bidder](indicators/ind001-Single-bidder.md)
- [002: Direct awards](indicators/ind002-Direct-awards.md)
- [003: Average tendering participation](indicators/Average-tendering.md)
- [004: Average tender length](indicators/Average-tender-length.md)
- [005: Average discount](indicators/Average-discount.md)

**004: Strategic Procurement**

- [001: Strategic procurement](indicators/strategic_procurement.md)
- [002: Accessibility](indicators/accessibility.md)

**005: SME participation**

- [001: Contracts awarded to SMEs](indicators/ind007-Contracts-awarded-to-SMEs.md)
- [002: SME participation (SME bids)](indicators/ind008-SME-participation.md)
- [003: Average SME participation rate](indicators/Average-SME-participation-rate.md)
- [004: SME award rate (in number of contracts)](indicators/SME-award-rate-(in-value).md)
- [005: SME contractors](indicators/SME-award-rate-(in-number-of-contracts).md)
- [006: Contracts awarded to SMEs where there was at least one bid submitted by a SME](indicators/SME-award-rate-(in-number-of-contracts)-where-there-was-at-least-one-bid-submitted-by-a-SME.md)
- [007: SME award rate (in value) where there was at least one bid submitted by a SME](indicators/SME-award-rate-(in-value)-where-there-was-at-least-one-bid-submitted-by-a-SME.md)

**006: Efficiency**

- [001: Joint procurement](indicators/ind004-Joint-procurement.md)
- [002: Award based only on price](indicators/ind005-Award-criteria-based-on-price-alone.md)
- [003: Decision speed](indicators/ind006-Decision-speed.md)

**007: Cross-border procurement**

*No indicators defined for the time being.*

**Common Rules**

- [Data Quality Common Rules](indicators/common_quality_rules.md)

This Business Rules are common to all Data Quality Indicators.


### Providing feedback or reporting issues
If you have any feedback or wish to report an issue related to the indicators:

1. **Open an Issue:** Please open an issue on our GitLab repository [here](https://code.europa.eu/eproc/ppds/crs/-/issues/new). 
2. **Tag the Issue:** When creating the issue, include the corresponding Group number, Indicator number, and the Indicator name in the issue title to ensure clarity.
   - For example, if you're reporting an issue with the "Missing link to call for competition" indicator in the "Data Quality" group, the issue title should be formatted as: `Group [001], Indicator [001] - Missing link to call for competition - Issue in calculation`.

All the issues and feedback regarding the indicators are available in the [Change Requests repository](https://code.europa.eu/eproc/ppds/crs/-/issues).



### Go back to Public Procurement Data Space (PPDS) [Home](index.md).




