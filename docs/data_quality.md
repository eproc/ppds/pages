## **PPDS Data Quality Tools**

This repository provides documentation and information regarding the data quality rules applied within the PPDS to ensure the good quality of the data regarding validity, completeness, consistency, accuracy, uniqueness, and timeliness.

This repository is structured within the following pages:

* [Completeness](data_quality/Completeness.md). Data completeness is a measure of **how much of the required or expected data is present within a dataset**.
* [Validity](data_quality/Validity.md). Data validity assures that **data** should be collected **according to defined business rules and parameters** and should conform to the **correct format** and **fall within the correct range**.
* [Consistency](data_quality/Consistency.md). Data consistency refers to the **uniformity and coherence** of data within and across different datasets.
* [Accuracy](data_quality/Accuracy.md). Data accuracy is the extent to which data reflects **actual and real-world scenarios**.
* [Uniqueness](data_quality/Uniqueness.md). Data uniqueness ensures there are **no duplications or overlapping of values across all datasets**.
* [Timeliness](data_quality/Timeliness.md). Data timeliness refers to the degree to which **data is available and up to date**.

The pages provide information about the different data quality rules implemented in the PPDS, including the data fields identified will determine the compliance of the data. In the PPDS, no data is discarded for data quality reasons, as a data quality assessment for validation purposes is performed.

In order to report any feedback or comments on the data quality rules above, please, open an issue [here](https://code.europa.eu/eproc/ppds/crs/-/issues/new).

All the issues and feedback regarding the data quality rules are available in the [Change Requests repository](https://code.europa.eu/eproc/ppds/crs/-/issues).

Go back to Public Procurement Data Space (PPDS) [Home](index.md).



