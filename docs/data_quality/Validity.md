Data validity assures that **data** should be collected **according to defined business rules and parameters** and should conform to the **correct format** and **fall within the correct range**.

The following table provides a list of Data Quality rules to check data validity:

| **ID**          | **Business Rule**                                                                                     | **Applicable for the values of the ePO property (ePO v3.1.0)**                      |
|-----------------|-------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| VAL004          | Country code is valid as defined by EU Vocabularies (“Code” column)                                    | `http://data.europa.eu/a4g/ontology#hasCountryCode`                                 |
| VAL005          | Procedure type is valid as defined by EU Vocabularies (“Code” column)                                  | `http://data.europa.eu/a4g/ontology#hasProcedureType`                               |
| VAL006          | Contract nature type is valid as defined by EU Vocabularies (“Code” column)                            | `http://data.europa.eu/a4g/ontology#hasContractNatureType`                          |
| VAL007          | Main classification (CPV code) is valid as defined by EU Vocabularies                                  | `http://data.europa.eu/a4g/ontology#hasMainClassification`                          |
| VAL008          | NUTS code is valid as defined by EU Vocabularies                                                       | `http://data.europa.eu/a4g/ontology#hasNutsCode`                                    |
| VAL009          | Legal basis is valid as defined by EU Vocabularies (“Code” column)                                     | `http://data.europa.eu/a4g/ontology#hasLegalBasis`                                  |
| VAL010          | Currency code is valid as defined by EU Vocabularies (“Code” column)                                   | `http://data.europa.eu/a4g/ontology#hasCurrency`                                    |
| VAL011 + VAL012 | An amount is a number with or without decimals and ≥ 10                                                | `http://data.europa.eu/a4g/ontology#hasAmountValue`                                 |
| VAL013          | Estimated value is an amount                                                                           | `http://data.europa.eu/a4g/ontology#hasEstimatedValue` (CN, CAN and lot levels)     |
| VAL028          | Awarded value is an amount                                                                             | `http://data.europa.eu/a4g/ontology#hasAwardedValue` (at lot level)                 |
| VAL014          | Total awarded value is an amount                                                                       | `http://data.europa.eu/a4g/ontology#hasTotalAwardedValue` (at CAN level)            |
| VAL015          | Number of received tenders is an integer                                                               | `http://data.europa.eu/a4g/ontology#hasReceivedTenders`                             |
| VAL016          | Number of received tenders is ≥ 0                                                                      | `http://data.europa.eu/a4g/ontology#hasReceivedTenders`                             |
| VAL017          | Number of received SME tenders is an integer                                                           | `http://data.europa.eu/a4g/ontology#hasReceivedSMETenders`                          |
| VAL018          | Number of received SME tenders is ≥ 0                                                                  | `http://data.europa.eu/a4g/ontology#hasReceivedSMETenders`                          |
| VAL019          | The contractor is an SME (business size) is valid as defined by EU Vocabularies (“Code” column)        | `http://data.europa.eu/a4g/ontology#hasBusinessSize`                                |
| VAL020          | Is joint procurement follows a Boolean format                                                          | `http://data.europa.eu/a4g/ontology#isJointProcurement`                             |
| VAL022          | Tender subcontracting information is valid as defined by EU Vocabularies (“Code” column)               | `http://data.europa.eu/a4g/ontology#hasTenderSubcontractingInformation`             |
| VAL023          | Award Criteria Weight is ≥ 0                                                                           | `http://data.europa.eu/m8g/#weight`                                                 |
| VAL024          | Award Criteria Weight is ≤ 100                                                                         | `http://data.europa.eu/m8g/#weight`                                                 |
|                 | Proper date format is YYYY-MM-DD                                                                       |                                                                                     |
| VAL025          | Dispatch date has a proper date format                                                                 | `http://data.europa.eu/a4g/ontology#hasDispatchDate`                                |
| VAL026          | Tenders receipt deadline has a proper date format                                                      | `http://data.europa.eu/a4g/ontology#hasReceiptDeadline`                             |
| VAL027          | Contract conclusion date has a proper date format                                                      | `http://data.europa.eu/a4g/ontology#hasContractConclusionDate`                      |
