Data timeliness refers to the degree to which **data is available and up to date**.

The following table provides a list of Data Quality rules to check data timeliness:

| **ID**   | **Business Rule**                                                                                          | **Applicable for the values of the ePO property (ePO v3.1.0)**                         |
|----------|------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| TIM001   | Information should be ingested in the PPDS no later than a given number of days after the publication of the notice | `http://data.europa.eu/a4g/ontology#hasDispatchDate`                                  |
| TIM002   | CAN should be published no later than a given number of days after the conclusion date                      | • Dispatch date: `http://data.europa.eu/a4g/ontology#hasDispatchDate`                 |
|          |                                                                                                             | • Conclusion of the contract: `http://data.europa.eu/a4g/ontology#hasContractConclusionDate` |
