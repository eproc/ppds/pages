Data completeness is a measure of **how much of the required or expected data is present within a dataset**.

The following table provides a list of Data Quality rules to check data completeness:

| **ID**   | **Business Rule**                                                   | **ePO property (ePO v3.1.0)**                                     | **Type of Notice**           |
| -------- | -------------------------------------------------------------------- | ------------------------------------------------------------------ | ---------------------------- |
| COM001   | Contract ID is provided                                              | `http://data.europa.eu/a4g/ontology#hasIdentifierValue (at contract level)` | Contract Award Notice (CAN)         |
| COM002   | Lot ID is provided                                                   | `http://data.europa.eu/a4g/ontology#hasIdentifierValue (at lot level)`      | Contract Notice (CN) & Contract Award Notice (CAN)         |
| COM003   | Legal name of the Buyer is provided                                  | `http://data.europa.eu/a4g/ontology#hasLegalName (at Buyer level)` | Contract Notice (CN) & Contract Award Notice (CAN)         |
| COM004   | National Registration Number of the Buyer is provided                | `http://data.europa.eu/a4g/ontology#hasIdentifierValue (at Buyer level)` | Contract Notice (CN)  & Contract Award Notice (CAN)        |
| COM005   | Main activity of the Buyer is provided                               | `http://data.europa.eu/a4g/ontology#hasMainActivity`               | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM006   | Legal type description of the Buyer is provided                      | `http://data.europa.eu/a4g/ontology#hasBuyerLegalTypeDescription`  | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM007   | Country code of the Buyer is provided                                | `http://data.europa.eu/a4g/ontology#hasCountryCode`                | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM030   | Legal Name of the Winner is provided                                 | `http://data.europa.eu/a4g/ontology#hasLegalName (at Winner level)`| Contract Award Notice (CAN)  |
| COM008   | National Registration Number of the Winner is provided               | `http://data.europa.eu/a4g/ontology#hasIdentifierValue (at Winner level)` | Contract Award Notice (CAN)  |
| COM009   | Country code of the Winner is provided                               | `http://data.europa.eu/a4g/ontology#hasCountryCode`                | Contract Award Notice (CAN)  |
| COM010   | Dispatch date  is provided                                           | `http://data.europa.eu/a4g/ontology#hasDispatchDate`               | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM011   | Legal basis is provided                                              | `http://data.europa.eu/a4g/ontology#hasLegalBasis`                 | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM013   | Procedure type is provided                                           | `http://data.europa.eu/a4g/ontology#hasProcedureType`              | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM014   | Contract nature type is provided                                     | `http://data.europa.eu/a4g/ontology#hasContractNatureType`         | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM015   | Main classification (CPV code) is provided                           | `http://data.europa.eu/a4g/ontology#hasMainClassification`         | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM016   | NUTS code is provided (Place of performance)                         | `http://data.europa.eu/a4g/ontology#hasNutsCode`                   | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM017   | Awarded value is provided                                            | `http://data.europa.eu/a4g/ontology#hasAwardedValue (at lot level)`| Contract Award Notice (CAN)  |
| COM029   | Total awarded value is provided                                      | `http://data.europa.eu/a4g/ontology#hasTotalAwardedValue (at CAN level)` | Contract Award Notice (CAN)  |
| COM018   | Currency code is provided                                            | `http://data.europa.eu/a4g/ontology#hasCurrency`                   | Contract Notice (CN) & Contract Award Notice (CAN)  |
| COM019   | Award criterion type is provided                                     | `http://data.europa.eu/a4g/ontology#hasAwardCriterionType`         | Contract Notice (CN) & Contract Award Notice (CAN)        |
| COM020   | Award Criteria Weight is provided                                    | `http://data.europa.eu/m8g/#weight`                                | Contract Notice (CN) & Contract Award Notice (CAN)         |
| COM021   | Tenders receipt deadline is provided                                 | `http://data.europa.eu/a4g/ontology#hasReceiptDeadline`            | Contract Notice (CN)         |
| COM022   | Contract conclusion date is provided                                 | `http://data.europa.eu/a4g/ontology#hasContractConclusionDate`     | Contract Award Notice (CAN)         |
| COM023   | Number of received tenders is provided                               | `http://data.europa.eu/a4g/ontology#hasReceivedTenders`            | Contract Award Notice (CAN)  |
| COM024   | Number of received SME tenders is provided                           | `http://data.europa.eu/a4g/ontology#hasReceivedSMETenders`         | Contract Award Notice (CAN)  |
| COM025   | Information on whether the contractor is an SME (business size) is provided | `http://data.europa.eu/a4g/ontology#hasBusinessSize`       | Contract Award Notice (CAN)  |
| COM026   | Tender subcontracting information is provided                        | `http://data.europa.eu/a4g/ontology#hasTenderSubcontractingInformation` | Contract Award Notice (CAN)  |
| COM027   | Information on whether the procurement scope is divided into lots is provided | `http://data.europa.eu/a4g/ontology#hasProcurementScopeDividedIntoLot` | Contract Notice (CN) & Contract Award Notice (CAN)      |
| COM028   | Information on whether the procurement procedure is joint procurement is provided | `http://data.europa.eu/a4g/ontology#isJointProcurement` | Contract Notice (CN) & Contract Award Notice (CAN)         |
