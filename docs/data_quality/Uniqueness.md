Data uniqueness ensures there are **no duplications or overlapping of values across all datasets**.  

In the context of the PPDS, the following situations should be taken into account:
- The same notice type for the same procurement procedure can be received from different data sources (for example, for procedures above the EU thresholds, notices are published both in TED and on the national platform).
- Different notice types can be published for a single procurement procedure throughout the process (for example, the Contract Notice and later on the Contract Award Notice).
- After a notice is published, subsequent corrections to this notice can be issued.

All these aforementioned situations are aligned with public procurement regulations and thus cannot be considered data quality issues. Therefore, since they are not data quality issues, uniqueness is not addressed through Data Quality business rules (i.e., SHACL validations) but rather in the Data Curation component of the PPDS Semantic Data Management. This component is responsible for:
- Identifying and linking all notices related to a single procurement procedure.
- Resolving duplicates or contradictory information received in different notices.

The need to define a Uniqueness Business Rule for cases where it is not possible to identify and link different notices related to a single procurement procedure can be analyzed in future stages of the PPDS.
