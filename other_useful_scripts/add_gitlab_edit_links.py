import os

# Define the root directory where your Markdown files are located
root_directory = "/Users/dani/Desktop/PPDS Gitlab pages/Test1/docs"

# Define the base GitLab repository URL
gitlab_repo_url = "https://gitlab.com/testprojects5994451/Test1"

# Function to add or replace "Edit on GitLab" link in a Markdown file
def add_or_replace_gitlab_edit_link(file_path):
    # Create the edit URL based on the file path
    relative_path = os.path.relpath(file_path, root_directory)
    edit_url = f"{gitlab_repo_url}/edit/master/docs/{relative_path.replace(' ', '%20')}"

    # Read the content of the file
    with open(file_path, "r") as file:
        content = file.read()

    # Remove any existing "Edit on GitLab" links
    content = content.replace("[Edit on GitLab](#", "[Old Edit Link](#")

    # Append the "Edit on GitLab" link at the end of the file
    with open(file_path, "a") as file:
        file.write(f"\n\n[Edit on GitLab]({edit_url})\n")

# Recursively traverse the directory and its subdirectories
for root, _, files in os.walk(root_directory):
    for file in files:
        if file.endswith(".md"):
            file_path = os.path.join(root, file)
            add_or_replace_gitlab_edit_link(file_path)

print("Edit links added or replaced in Markdown files.")
