from pathlib import Path
import re

def process_files(directory):
    path = Path(directory)
    for file_path in path.glob("*.md"):
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()

        # Replace Markdown bold with <span> tags to HTML <strong> tags
        content = re.sub(r'\*\*\s*<span dir="([^"]*)">([^<]+)</span>\s*\*\*', r'<strong><span dir="\1">\2</span></strong>', content)

        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(content)

directory = "docs/indicators"  # Adjust the path as necessary
process_files(directory)

print("All files have been processed to replace Markdown bold syntax with HTML <strong> tags.")
