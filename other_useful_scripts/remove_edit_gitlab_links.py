import os

# Define the root directory where your Markdown files are located
root_directory = "/Users/dani/Desktop/PPDS Gitlab pages/Test1/docs"

# Function to remove "Edit on GitLab" links from a Markdown file
def remove_gitlab_edit_links(file_path):
    # Read the content of the file
    with open(file_path, "r") as file:
        lines = file.readlines()

    # Remove lines containing "Edit on GitLab" links
    cleaned_lines = [line for line in lines if "[Edit on GitLab](" not in line]

    # Write the cleaned content back to the file
    with open(file_path, "w") as file:
        file.writelines(cleaned_lines)

# Recursively traverse the directory and its subdirectories
for root, _, files in os.walk(root_directory):
    for file in files:
        if file.endswith(".md"):
            file_path = os.path.join(root, file)
            remove_gitlab_edit_links(file_path)

print("Edit links removed from Markdown files.")
