import os

# Define the root directory where your Markdown files are located
root_directory = "/Users/dani/Desktop/PPDS Gitlab pages/Test1/docs"

# Function to clean up and add a single "Edit on GitLab" link in a Markdown file
def clean_and_add_gitlab_edit_link(file_path):
    # Read the content of the file
    with open(file_path, "r") as file:
        lines = file.readlines()

    # Create the edit URL based on the file path
    relative_path = os.path.relpath(file_path, root_directory)
    edit_url = f"[Edit on GitLab](https://gitlab.com/testprojects5994451/Test1/edit/master/docs/{relative_path.replace(' ', '%20')})\n"

    # Find and remove any existing "Edit on GitLab" links
    cleaned_lines = [line for line in lines if "[Edit on GitLab](" not in line]

    # Append the "Edit on GitLab" link at the end of the cleaned content
    cleaned_lines.append(edit_url)

    # Write the cleaned content back to the file
    with open(file_path, "w") as file:
        file.writelines(cleaned_lines)

# Recursively traverse the directory and its subdirectories
for root, _, files in os.walk(root_directory):
    for file in files:
        if file.endswith(".md"):
            file_path = os.path.join(root, file)
            clean_and_add_gitlab_edit_link(file_path)

print("Edit links cleaned and added in Markdown files.")
